//TODO: Make a function to add a new cat into an empty cats array
// Name the function addNewCat
let cats = [];

const addNewCat = (catName, catBreed, catGender, CatNeutered) => {
  let catObject = {
    name: catName,
    breed: catBreed,
    gender: catGender,
    neutered: CatNeutered,
  };
  cats.push(catObject);
  console.log(cats)
};

addNewCat("Tuna", "Siamese", "Female", true);
addNewCat("Mickey", "Siamese", "Male", true);

//1. Make a cats array
// console.log(cats);
// 2. Make a cat object

//   console.log(catObject);
//3. Put cat object into a function
// console.log(cats);
// 4. change the hard coded cat variables and call the function

//5. push to cats array
// const addNewCat = () => {
//   let cat = {
//     name: "Tuna",
//     breed: "Siamese",
//     gender: "Female",
//     neutered: true,
//   };
//   return cat;
// };

// let cats = [];
// cats.push(addNewCat());
// console.log(cats);
/**BONUS TODO:
 * Define the following function (numCatsInCafe)
 * which should return the number of cats in the cats array
 */

const numCatsInCafe = (catsArray) => {
  return catsArray.length;
};

let numOfCats = numCatsInCafe(cats);
console.log(numOfCats);
